#!/bin/bash

if [ "$#" -ne 6 ]; then
    echo "Usage: animate <input-image> <output-image> <frames> <fps> <shape> <shape-count>"
    exit 1
fi

outdir=`mktemp -d`

for n in `seq 0 $(($3 - 1))`; 
do 
	padn=`printf "%03d" $n`
	echo "Generating frame $n"
	primitive -i $1 -o "$outdir/$padn.png" -m $5 -n $6
done

ffmpeg -i "$outdir/%03d.png" -r $4 $2
