This is a small script for creating animated GIFs of images generated using [primitive](https://github.com/fogleman/primitive). For example:

![Aquarium animation](https://bytebucket.org/tpettersen/primitive-gif/raw/05d89ee33361f1329a0b1490b0b31c574760382e/aquarium-500.gif)

# Dependencies

- [primitive](https://github.com/fogleman/primitive) 
- [ffmpeg](https://www.ffmpeg.org/)

# Usage

```
./animate.sh <input-image> <output-image> <frames> <fps> <shape> <shape-count>
```

For example:

```
./animate.sh aquarium.png aquarium.gif 6 10 7 500
```

Will create a GIF with 6 frames each containing 500 rotated elipses approximating `aquarium.png`.

Shapes are 0=combo, 1=triangle, 2=rect, 3=ellipse, 4=circle, 5=rotatedrect, 6=beziers, 7=rotatedellipse, 8=polygon as per the [primitive docs](https://github.com/fogleman/primitive#command-line-usage).